// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

//const remote = require('electron').remote;

var maximized = false;
var lastWindowSizePos = {x: 0, y:0, width:800, height:600};
lastWindowSizePos.x = window.screenLeft;
lastWindowSizePos.y = window.screenTop;

function minimizeWin(){
	//var window = remote.getCurrentWindow();
	window.minimize();  
}

function closeWin(){
	//var window = remote.getCurrentWindow();
	window.close();
	app.quit();
}

function restoreMaxWindow() {
	console.log(lastWindowSizePos);
	//var window = remote.getCurrentWindow();
	if(!maximized){
		lastWindowSizePos.x = window.screenLeft;
		lastWindowSizePos.y = window.screenTop;
		lastWindowSizePos.width = window.width;
		lastWindowSizePos.width = window.height;
		window.moveTo(0, 0);
		window.resizeTo(screen.width, screen.height);
	}else{
		window.moveTo(lastWindowSizePos.x, lastWindowSizePos.y);
		window.resizeTo(lastWindowSizePos.width, lastWindowSizePos.height);
	}
	maximized = !maximized;
}